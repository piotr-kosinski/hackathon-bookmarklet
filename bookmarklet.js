var BOOKMARKLET = (function () {

    var LOG_BOX_CSS = 'position:fixed;width:200px;height:150px;background:black;color:white;left:auto;right:0px;top:0px;z-index:999999;font-size:10px;word-wrap:break-word';
    var VIDEO_BOX_CSS = 'display:none';
    var API_URL = 'http://193.187.65.48/app.php/feelings';

    var IMAGE_BE_HAPPY = 'http://193.187.65.48/images/dontworry_dog.jpg';
    var IMAGE_SO_SERIOUS = 'http://193.187.65.48/images/whysoserious-swatch-black.jpg';
    var IMAGE_DISCOUNT = 'http://193.187.65.48/images/emag-discount-popup.png';

    var VIDEO_WIDTH = 640;
    var VIDEO_HEIGHT = 480;

    var Detector = null;

    var init = function () {
        addCss();

        createLogBox();
        createVideoBox();
        downloadLibraries();
    };

    var addCss = function () {
        window.jQuery("head").append("<link id='magnificPopup' href='https://cdn.rawgit.com/dimsemenov/Magnific-Popup/master/dist/magnific-popup.css' type='text/css' rel='stylesheet' />");
    }

    var addImages = function () {
        window.jQuery('body').append('<a style="display: none" id="beHappy" href="'+IMAGE_BE_HAPPY+'" /></a>');
        window.jQuery('body').append('<a style="display: none" id="soSerious" href="'+IMAGE_SO_SERIOUS+'" /></a>');
        window.jQuery('body').append('<a style="display: none" id="discountPopup" href="'+IMAGE_DISCOUNT+'" /></a>');

        window.jQuery('#beHappy, #soSerious, #discountPopup').magnificPopup({
            type: 'image'
        });
    }

    var logInfo = function (log) {
        window.jQuery('#FDLogBox').html('<p>'+log+'</p>');
    };

    var createLogBox = function () {
        window.jQuery('body').append('<div id="FDLogBox"></div>');
        window.jQuery('#FDLogBox').attr('style', LOG_BOX_CSS)
        window.jQuery('head').append('<style>#FDLogBox:before { content: "Log box" }</style>');
        return true;
    };

    var createVideoBox = function () {
        window.jQuery('body').append('<div id="FDVideoBox"></div>');
        window.jQuery('#FDVideoBox').attr('style', VIDEO_BOX_CSS)
    };

    var downloadLibraries = function () {

        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "https://download.affectiva.com/js/3.1/affdex.js";
        $("head").append(s);

        window.jQuery.when(
            window.jQuery.getScript( "http://193.187.65.48/js/withinviewport.js" ),
            window.jQuery.getScript( "http://193.187.65.48/js/jquery.withinviewport.js" ),
            window.jQuery.getScript( "https://download.affectiva.com/js/3.1/affdex.js" ),
            window.jQuery.getScript( "https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js" ),
            window.jQuery.Deferred(function ( deferred ){
                window.jQuery( deferred.resolve );
            })
        ).done(function (){
            logInfo('Files are loaded!');
            startRecording();
            addImages();
        });
    };

    var startRecording = function () {
        Detector = new affdex.CameraDetector(window.jQuery("#FDVideoBox")[0], VIDEO_WIDTH, VIDEO_HEIGHT);
        Detector.detectAllEmotions();
        Detector.detectAllExpressions();
        Detector.addEventListener("onInitializeSuccess", function () {
            logInfo("The detector reports initialized");
        });
        Detector.addEventListener("onWebcamConnectSuccess", function () {
            logInfo("Webcam access allowed");
        });

        Detector.addEventListener("onImageResultsSuccess", function (faces, image, timestamp) {
            if (faces.length > 0) {
                visibleElements = window.jQuery('#main-container section div[id]').withinviewport().map(function (index,dom){return dom.id});
                visibleElementsString = visibleElements.toArray().join(",");

                emotionsJson = JSON.stringify(faces[0].emotions, function (key, val) {
                    return val.toFixed ? Number(val.toFixed(0)) : val;
                });

                expressionsJson = JSON.stringify(faces[0].expressions, function (key, val) {
                    return val.toFixed ? Number(val.toFixed(0)) : val;
                });

                logInfo(
                    "Emotions: " + emotionsJson +
                    "<br />Expressions: " + expressionsJson
                );

                handleTriggers(faces[0].emotions, faces[0].expressions);

                sendData({'emotions': emotionsJson, 'expressions': expressionsJson}, visibleElementsString)
            }
        });

        //start detector
        Detector.start();
    };

    var sendData = function (data, elements) {
        window.jQuery.post(API_URL, {"data": data, "elements": elements}, function (){
            console.log('Data saved');
        }, "json");
    }

    var handleTriggers = function (emotions, expressions) {
        // Prevent to show two popups on the same time
        console.log(window.jQuery('.mfp-content').length);
        if (window.jQuery('.mfp-content').length == 0) {
            if (emotions.sadness > 30) {
                window.jQuery('#beHappy').click();

                return true;
            }

            if (emotions.contempt > 98) {
                window.jQuery('#soSerious').click();

                return true;
            }

            if (expressions.browRaise > 70) {
                window.jQuery('#discountPopup').click();

                return true;
            }

        } else {
            console.log ('Skip triggers');
        }
    }

    return {
        init: init
    }
})();



